package ru.rustore.godot.review

import android.util.ArraySet
import com.google.gson.Gson
import org.godotengine.godot.Godot
import org.godotengine.godot.plugin.GodotPlugin
import org.godotengine.godot.plugin.SignalInfo
import org.godotengine.godot.plugin.UsedByGodot
import ru.rustore.godot.core.JsonBuilder
import ru.rustore.sdk.core.exception.RuStoreException
import ru.rustore.sdk.review.RuStoreReviewManager
import ru.rustore.sdk.review.RuStoreReviewManagerFactory
import ru.rustore.sdk.review.model.ReviewInfo

class RuStoreGodotReview(godot: Godot?): GodotPlugin(godot) {
    private companion object {
        const val PLUGIN_NAME = "RuStoreGodotReview"
        const val CHANNEL_REQUEST_REVIEW_FLOW_SUCCESS = "rustore_request_review_flow_success"
        const val CHANNEL_REQUEST_REVIEW_FLOW_FAILURE = "rustore_request_review_flow_failure"
        const val CHANNEL_LAUNCH_REVIEW_FLOW_SUCCESS = "rustore_launch_review_flow_success"
        const val CHANNEL_LAUNCH_REVIEW_FLOW_FAILURE = "rustore_launch_review_flow_failure"
        const val UNKNOWN_ERROR = "Unknown error"
    }

    override fun getPluginName(): String {
        return PLUGIN_NAME
    }

    override fun getPluginSignals(): Set<SignalInfo> {
        val signals: MutableSet<SignalInfo> = ArraySet()
        signals.add(SignalInfo(CHANNEL_REQUEST_REVIEW_FLOW_SUCCESS))
        signals.add(SignalInfo(CHANNEL_REQUEST_REVIEW_FLOW_FAILURE, String::class.java))
        signals.add(SignalInfo(CHANNEL_LAUNCH_REVIEW_FLOW_SUCCESS))
        signals.add(SignalInfo(CHANNEL_LAUNCH_REVIEW_FLOW_FAILURE, String::class.java))

        return signals
    }

    private lateinit var reviewManager: RuStoreReviewManager
    private var isInitialized: Boolean = false
    private var reviewInfo: ReviewInfo? = null

    @UsedByGodot
    fun init(metricType: String) {
        if (isInitialized) return

        godot.getActivity()?.run {
            reviewManager = RuStoreReviewManagerFactory.create(
                context = application,
                internalConfig = mapOf("type" to metricType)
            )
            isInitialized = true
        }
    }

    @UsedByGodot
    fun requestReviewFlow() {
        if (!isInitialized) {
            val throwable = RuStoreException(UNKNOWN_ERROR)
            emitSignal(CHANNEL_REQUEST_REVIEW_FLOW_FAILURE, JsonBuilder.toJson(throwable))
            return
        }

        reviewManager.requestReviewFlow()
            .addOnSuccessListener { result ->
                reviewInfo = result
                emitSignal(CHANNEL_REQUEST_REVIEW_FLOW_SUCCESS)
            }
            .addOnFailureListener { throwable ->
                emitSignal(CHANNEL_REQUEST_REVIEW_FLOW_FAILURE, JsonBuilder.toJson(throwable))
            }
    }

    @UsedByGodot
    fun launchReviewFlow() {
        if (!isInitialized) {
            val throwable = RuStoreException(UNKNOWN_ERROR)
            emitSignal(CHANNEL_LAUNCH_REVIEW_FLOW_FAILURE, JsonBuilder.toJson(throwable))
            return
        }

        reviewInfo?.let {
            reviewManager.launchReviewFlow(reviewInfo = it)
                .addOnSuccessListener {
                    emitSignal(CHANNEL_LAUNCH_REVIEW_FLOW_SUCCESS)
                }
                .addOnFailureListener { throwable ->
                    emitSignal(CHANNEL_LAUNCH_REVIEW_FLOW_FAILURE, JsonBuilder.toJson(throwable))
                }
        }
    }
}
