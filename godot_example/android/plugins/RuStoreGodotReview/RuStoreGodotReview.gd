# RuStoreGodotReviewManager
# @brief
#	Класс для работы с оценками и отзывами.
#	Предоставляет API для запуска UI-формы, позволяющей пользователю оставить оценку и отзыв о вашем приложении в "RuStore".
class_name RuStoreGodotReviewManager extends Object

const SINGLETON_NAME = "RuStoreGodotReview"
const METRIC_TYPE = "godot"

# @brief Возвращает true, если синглтон инициализирован, в противном случае — false.
var isInitialized: bool = false
var _clientWrapper: Object = null

static var _instance: RuStoreGodotReviewManager = null

# @brief Действие, выполняемое при успешном завершении request_review_flow.
signal on_request_review_flow_success

# @brief Действие, выполняемое в случае ошибки request_review_flow.
signal on_request_review_flow_failure

# @brief Действие, выполняемое при успешном завершении launch_review_flow.
signal on_launch_review_flow_success

# @brief Действие, выполняемое в случае ошибки launch_review_flow.
signal on_launch_review_flow_failure

# @brief
#	Получить экземпляр RuStoreGodotReviewManager.
# @return
#	Возвращает указатель на единственный экземпляр RuStoreGodotReviewManager (реализация паттерна Singleton).
#	Если экземпляр еще не создан, создает его.
static func get_instance() -> RuStoreGodotReviewManager:
	if _instance == null:
		_instance = RuStoreGodotReviewManager.new()
	return _instance


func _init():
	if Engine.has_singleton(SINGLETON_NAME):
		_clientWrapper = Engine.get_singleton(SINGLETON_NAME)
		_clientWrapper.rustore_request_review_flow_success.connect(_on_request_review_flow_success)
		_clientWrapper.rustore_request_review_flow_failure.connect(_on_request_review_flow_failure)
		_clientWrapper.rustore_launch_review_flow_success.connect(_on_launch_review_flow_success)
		_clientWrapper.rustore_launch_review_flow_failure.connect(_on_launch_review_flow_failure)
		
		_clientWrapper.init(METRIC_TYPE)
		isInitialized = true


# @brief
#	Выполняет запуск формы для запроса оценки и отзыва у пользователя.
#	Каждому вызову метода должен предшествовать вызов RequestReviewFlow.
func request_review_flow():
	_clientWrapper.requestReviewFlow()

func _on_request_review_flow_success():
	on_request_review_flow_success.emit()
	
func _on_request_review_flow_failure(json: String):
	var error = RuStoreError.new(json)
	on_request_review_flow_failure.emit(error)


# @brief
#	Выполняет запуск формы для запроса оценки и отзыва у пользователя.
#	Каждому вызову метода должен предшествовать вызов RequestReviewFlow.
func launch_review_flow():
	_clientWrapper.launchReviewFlow()

func _on_launch_review_flow_success():
	on_launch_review_flow_success.emit()
	
func _on_launch_review_flow_failure(json: String):
	var error = RuStoreError.new(json)
	on_launch_review_flow_failure.emit(error)
