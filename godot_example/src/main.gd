extends Node

@onready var _loading = $LoadingPanel

var _core_client: RuStoreGodotCoreUtils = null
var _review_client: RuStoreGodotReviewManager = null

# Called when the node enters the scene tree for the first time.
func _ready():
	_core_client = RuStoreGodotCoreUtils.get_instance()
	_review_client = RuStoreGodotReviewManager.get_instance()
	_review_client.on_request_review_flow_success.connect(_on_request_review_flow_success)
	_review_client.on_request_review_flow_failure.connect(_on_request_review_flow_failure)
	_review_client.on_launch_review_flow_success.connect(_on_launch_review_flow_success)
	_review_client.on_launch_review_flow_failure.connect(_on_launch_review_flow_failure)


func _on_request_review_flow_button_pressed():
	_loading.visible = true
	_review_client.request_review_flow()

func _on_request_review_flow_success():
	_loading.visible = false
	_core_client.show_toast("Request review flow OK")

func _on_request_review_flow_failure(error: RuStoreError):
	_loading.visible = false
	_core_client.show_toast(error.description)


func _on_launch_review_flow_button_pressed():
	_review_client.launch_review_flow()

func _on_launch_review_flow_success():
	_core_client.show_toast("Launch review flow OK")

func _on_launch_review_flow_failure(error: RuStoreError):
	_core_client.show_toast(error.description)
